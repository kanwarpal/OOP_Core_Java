/**
 * 
 * @author kanwarpalmehra
 *
 */
public class AddressEntry {

	String firstname;
	String lastname;
	String street;
	String city;
	String state;
	Integer zip;
	String phone;
	String email;
	

	/**
	 * 
	 * @param firstname
	 * @param lastname
	 * @param street
	 * @param city
	 * @param state
	 * @param zip
	 * @param phone
	 * @param email
	 * 
	 * a constructor initialising 
	 */
	public AddressEntry(String firstname,String lastname,String street,String city,String state,Integer zip,String phone, String email){
		this.firstname=firstname;
		this.lastname=lastname;
		this.street=street;
		this.city=city;
		this.state=state;
		this.zip=zip;
		this.phone=phone;
		this.email=email;
		
		
		
	}

	public AddressEntry() {
		// TODO Auto-generated constructor stub
	}

/**
 * toString method concatenates all fields in a string 
 */
	public String toString(){
		
			return new String("FirstName:"+getFirstname()+"\n"+
								"LastName:"+getLastname()+"\n"+
								  "Street:"+getState()+"\n"+
									"City:"+getCity()+"\n"+
								  	  "State:"+getState()+"\n"+
										"Zip:"+getZip()+"\n"+
								  	  	  "Phone:"+getPhone()+"\n"+
								  	  	  	"Email:"+getEmail()+"\n");
											}
	
/**
 * getters and setters of all fields
 * @return
 */
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		
		return zip.toString();
	}

	public void setZip(Integer zip) {
		this.zip = zip;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
