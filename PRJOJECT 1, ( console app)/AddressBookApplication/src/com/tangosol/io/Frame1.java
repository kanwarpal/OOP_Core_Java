package com.tangosol.io;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
//import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class Frame1 {

	private JFrame frame;
	private JTextField textFieldNum1;
	private JTextField textFieldNum2;
	private JButton btnNewButtonAdd;
	private JButton btnNewButtonSub;
	private JTextField textFieldAns;
	private JLabel lblResult;
	private JLabel lblEnterNum;
	private JLabel lblNewLabel;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame1 window = new Frame1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Frame1() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textFieldNum1 = new JTextField();
		textFieldNum1.setBounds(27, 37, 130, 26);
		frame.getContentPane().add(textFieldNum1);
		textFieldNum1.setColumns(10);
		
		textFieldNum2 = new JTextField();
		textFieldNum2.setBounds(27, 91, 130, 26);
		frame.getContentPane().add(textFieldNum2);
		textFieldNum2.setColumns(10);
		
		btnNewButtonAdd = new JButton("ADD");
		btnNewButtonAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int num1 , num2,ans;
			try{
				num1=Integer.parseInt(textFieldNum1.getText());
				num2=Integer.parseInt(textFieldNum2.getText());
				
				ans=num1+num2;
				textFieldAns.setText(Integer.toString(ans));
			}
			catch (Exception e1){
				JOptionPane.showConfirmDialog(null, "enter numbers");
			}
			
			
			
			
			
			
			}
		});
		btnNewButtonAdd.setBounds(61, 146, 117, 29);
		frame.getContentPane().add(btnNewButtonAdd);
		
		btnNewButtonSub = new JButton("SUB");
		btnNewButtonSub.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			int num3,num4,answer;
				try{
						num3=Integer.parseInt(textFieldNum1.getText());
						num4=Integer.parseInt(textFieldNum2.getText());
						 answer= num3-num4;
						 
						 textFieldAns.setText(Integer.toString(answer));
					
			}catch(Exception e2){
				JOptionPane.showConfirmDialog(null, "enter nums only");
			}
			}
		});
		btnNewButtonSub.setBounds(61, 187, 117, 29);
		frame.getContentPane().add(btnNewButtonSub);
		
		textFieldAns = new JTextField();
		textFieldAns.setBounds(27, 246, 130, 26);
		frame.getContentPane().add(textFieldAns);
		textFieldAns.setColumns(10);
		
		lblResult = new JLabel("RESULT");
		lblResult.setBounds(191, 251, 61, 16);
		frame.getContentPane().add(lblResult);
		
		lblEnterNum = new JLabel("Enter Num 1");
		lblEnterNum.setBounds(173, 42, 89, 16);
		frame.getContentPane().add(lblEnterNum);
		
		lblNewLabel = new JLabel("Enter Num 2");
		lblNewLabel.setBounds(172, 96, 80, 16);
		frame.getContentPane().add(lblNewLabel);
	}
}
