/**
 * @author kanwarpalmehra
 * @date oct, 2016
 * AddressBook console application with a menu of functions.
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.io.IOException;
import java.util.Scanner;

public class AddressBookApplication {
	
	public static Scanner input;
	//static AddressBook ab=new AddressBook();
	
	static AddressBook new_add_book = new AddressBook();
	
	public static AddressEntry new_add_entry = new AddressEntry();
	 
	public void intiAddressBookExercise (AddressBook new_add_book)
	{
		AddressEntry ae1=new AddressEntry("Kanwar","Mehra","whitman","Hayward","CA",94544,"946311700","kanwarriors@gmail.com");
		AddressEntry ae2=new AddressEntry("pal","Mehras","WHITMAN","HY","CAL",94545,"337373","kanwarsgmail.com");
		new_add_book.add(ae1);
		new_add_book.add(ae2);
				
		new_add_book.add(new_add_entry);
		
	}
	
/**
 * 
 * @param args
 * Main method 
 */
	public static void main(String[] args) 
										{
												
												Menu();
												
										}
		
	/**
	 * Menu method , presents choose-up menu with switch cases
	 */
	public static void Menu()
			{
			Menu obj = new Menu();
	
			System.out.println(obj.Menu_Display());
			input = new Scanner(System.in);
			String selection = input.nextLine();
			System.out.println("YOU SELECTED : " + selection);
			switch (selection) {
								case "a":
									load();
									Menu();
									break;
								
								case "b":
									Add();
									Menu();
									break;
								
								case "c":
									System.out.println("GIVE LASTNAME OF CONTACT YOU WANT TO REMOVE: \n");
									String removekey=input.nextLine();
									new_add_book.remove(removekey);
									Menu();
									break;
									
								case "d":
									System.out.println("SEARCH BY LASTNAME: \n");
									String lastName_key = input.nextLine();
									new_add_book.find(lastName_key);
									Menu();
									break;
									
								case "e":
									new_add_book.list();
									Menu();
									break;
								case "f":
									System.out.println("<<<<THANKS>>>");;
									System.exit(1);
									Menu();
									break;
								
								default:
									System.out.println("Invalid Menu..!!");
									Menu();
									break;	
									
								}
			}
	/**
	 * load method read a .txt file with intially installed addressEntries
	 */
	public static void load()
			{
				String fileName = "Datafile.txt";
	
		        String line = null;
		
		        try {
		           
		            FileReader fileReader = new FileReader(fileName);
		
		            BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		            while((line = bufferedReader.readLine()) != null) 
		            {
		                System.out.println(line);
		            }   
		
		            
		            bufferedReader.close();         
		            }
		        
		        catch(FileNotFoundException ex) 
		        	{
		            System.out.println("Unable to open file '" + fileName + "'");                
		            }
		        catch(IOException ex) 
		        	{
		            System.out.println("Error reading file '" + fileName + "'");                  
		            }	
				}
	
	/**
	 * Add method takes input of every individual field and calls constructor for cr
	 */
	public static void Add()
				{
					String firstname;
					String lastname;
					String street;
					String city;
					String state;
					Integer zip;
					String phone;
					String email;
					
					input = new Scanner(System.in);
					System.out.println("Enter following details to create a new addressentry");
					Menu.prompt_FirstName();
					firstname=input.nextLine();
					
					Menu.prompt_LastName();
					lastname=input.nextLine();
					
					Menu.prompt_Street();
					street=input.nextLine();
				
					Menu.prompt_City();
					city=input.nextLine();
					
					Menu.prompt_State();
					state=input.nextLine();
					
					Menu.prompt_Zip();
					zip=input.nextInt();
					
					Menu.prompt_Telephone();
					phone=input.next();
					
					Menu.prompt_Email();
					email=input.next();
			
					new_add_entry = new AddressEntry(firstname, lastname, street, city, state, zip,phone,email);
					new_add_book.add(new_add_entry);
				}

}
