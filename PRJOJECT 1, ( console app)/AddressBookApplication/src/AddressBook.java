/**
 * @author kanwarpalmehra
 * 
 */
import java.util.ArrayList;
import java.util.Iterator;

public class AddressBook  {
    
	public ArrayList<AddressEntry> addressEntryList = new ArrayList<AddressEntry>();
	
    public void add(AddressEntry ae) 
		{
		addressEntryList.add(ae);
		}
	
	
	
	/**
	 * list , prints out whole address book 
	 */
    public void list()
		{
			Iterator<AddressEntry> itr1 = addressEntryList.iterator();
			while(itr1.hasNext())
			 {
				itr1.next(); 
				System.out.println("\n"+addressEntryList.toString());
				System.out.println("****************************\n\n");
			 };
		}
	
	/**
	 * 
	 * @param lastName
	 * find method searches for a address entry with provided last name
	 * 
	 */
    public void find(String lastName) 
		{
			
			ArrayList<String> key = new ArrayList<String>();
			
			for (AddressEntry new_add_entry : addressEntryList) 
				
				{
					if (new_add_entry.getLastname().toLowerCase().matches(lastName.toLowerCase() + ".*")) 
						{
							key.add(new_add_entry.getLastname());
							System.out.println(new_add_entry.toString());
						}
				}
		}
	
	/**
	 * 
	 * @param lastName
	 * remove method removes the address entry matching with param lastname
	 */
    public void remove(String lastName) 
		{
				for (AddressEntry new_add_entry : addressEntryList) 
					{
						if (new_add_entry.getLastname().equalsIgnoreCase(lastName)) 
							{
							addressEntryList.remove(new_add_entry);
							System.out.println("You removed \n" + new_add_entry.toString());
							}
					}
				
		}
	

	
}
