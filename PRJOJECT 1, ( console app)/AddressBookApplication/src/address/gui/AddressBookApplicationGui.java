package address.gui;

import java.awt.EventQueue;
import java.awt.Window;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTable;

import net.proteanit.sql.DbUtils;

import javax.swing.JScrollPane;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;




public class AddressBookApplicationGui {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AddressBookApplicationGui window = new AddressBookApplicationGui();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	
	Connection connection=null;
	//private JTable table;
	
	public AddressBookApplicationGui() {
		
	
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Display");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
			try{
				Class.forName ("oracle.jdbc.OracleDriver");
				Connection connection = 
						(Connection) DriverManager.getConnection("jdbc:oracle:thin:bv4228/0m6EKYTa@mcsdb1.sci.csueastbay.edu:1521/mcsdb1");
							JOptionPane.showConfirmDialog(null,"connection successful");
				
							Statement stmt = connection.createStatement();
				

				 

				// Select the all (*) from the table JAVATEST

				ResultSet rset = stmt.executeQuery("SELECT * FROM ADDRESSENTRYTABLE");
					
				table.setModel(DbUtils.resultSetToTableModel(rset));
				
			}
			catch(Exception e2){
				
			}
			
			}
		});
		btnNewButton.setBounds(20, 57, 117, 29);
		frame.getContentPane().add(btnNewButton);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(188, 42, 456, 416);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		JButton btnNewButton_1 = new JButton("Enter");
		btnNewButton_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
			
				try{
					
					frame.dispose();
					//enter obj= new enter();
				//	  obj.setVisible(true);
					
				}
				catch(Exception e3){
					JOptionPane.showConfirmDialog(null, "CANT go to ENtER window");
				}
			
			}
		});
		btnNewButton_1.setBounds(20, 108, 117, 29);
		frame.getContentPane().add(btnNewButton_1);
	}
}
