import java.io.*;
class Menu
{
	static void prompt_FirstName()
		{
			System.out.println("First Name:");
		}
	static void prompt_LastName()
		{
			System.out.println("Last Name:");
		}	
	static void prompt_Street()
		{
			System.out.println("Street:");
		}
	static void prompt_City()
		{
			System.out.println("City:");
		}	
	static void prompt_State()
		{
			System.out.println("State:");
		}		
	static void prompt_Zip()
		{
			System.out.println("Zip:");
		}
	static void prompt_Telephone()
		{
			System.out.println("Telephone:");
		}		
	static void prompt_Email()
		{
			System.out.println("Email:");
		}	
}

public class AddressBookApplication
{
	public static void main (String args[] ){

		Menu.prompt_FirstName();
		Menu.prompt_LastName();
		Menu.prompt_Street();
		Menu.prompt_City();
		Menu.prompt_State();
		Menu.prompt_Zip();
		Menu.prompt_Telephone();
		Menu.prompt_Email();
	}
}